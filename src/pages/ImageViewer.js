import { useEffect, useState } from "react";
import axios from '../utils/axios'
import DrawBoard from "./DrawBoard";

export default function ImageViewer({alert}){
    const [url, setUrl] = useState(null);
    const [loading, setLoading] = useState(false)
    const [uploading, setUpLoading] = useState(false)
    const [selectedFile, setSelectedFile] = useState(null)

    useEffect(() => {
        fetchImageUrl()
    }, [])

    function fetchImageUrl(){
        setLoading(true)
        axios.get('/file-url')
        .then(res => {
            const {url} = res.data.data

            setUrl(url)
        })
        .catch(err => console.log(err))
        .finally(() => setLoading(false))
    }

    function uploadFile(){
        if(!selectedFile) return
        const formData = new FormData()

        formData.append('file', selectedFile)
        setUpLoading(true)
        axios.post('/file-upload', formData)
        .then(res => {
            if(!res.data.data){
                alert(res?.data?.message || "Something went wrong")
            }else{
                const {url} = res.data.data
                setUrl(url)
            }
        })
        .catch(err => console.log(err))
        .finally(() => {
            setUpLoading(false)
        })
    }

    if(loading){
        return (
            <div style={{width: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
                <span>Fetching Image</span>
            </div>
        )
    }

    return (
        <div style={{width: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center', flexDirection: 'column'}}>
            <span>
                <input type="file" onChange={e => setSelectedFile(e.target.files[0])}/>
                <button disabled={uploading} onClick={uploadFile}>
                    {uploading ? 'Uploading' : 'Upload'}
                </button>
            </span>
            <DrawBoard imgUrl={url} />
        </div>
    )
}