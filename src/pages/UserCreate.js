import '../App.css';
import {useState} from 'react'

export default function UserCreate({saveUser}){
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [email, setEmail] = useState('')

    function submitData(event){
        event.preventDefault();
        if(!firstName || !lastName || !email) return

        saveUser({
        first_name: firstName,
        last_name: lastName,
        email
        }, resetForm)
    }

    function resetForm(){
        setFirstName(''); setLastName(''); setEmail('')
    }

    return (
        <div style={{display: 'flex', alignItems: 'center'}}>
        <div className='User-form'>
        <form onSubmit={submitData}>
            <div>
            <h3>Registration</h3>
            </div>
            <div>
            <label htmlFor='first_name'>First Name</label><br/>
            <input type='text' name='first_name' id='first_name' value={firstName} onChange={e => setFirstName(e.target.value)}/>
            </div>
            
            <div>
            <label htmlFor='last_name'>Last Name</label><br/>
            <input type='text' name='last_name' id='last_name' value={lastName} onChange={e => setLastName(e.target.value)}/>
            </div>

            <div>
            <label htmlFor='email'>Email</label><br/>
            <input type='email' name='email' id='email' value={email} onChange={e => setEmail(e.target.value)}/>
            </div>

            <button type='submit' className='expanded-btn'>Save</button>
            <button onClick={resetForm} className='expanded-btn'>Reset</button>
        </form>
        </div>
        </div>
    )
}