import ImageViewer from "./ImageViewer"

export default function UserList(props){
    const {users, deleteUser, toggleSelected} = props

    function deleteBulkUsers(){
        const ids = users.filter(user => user.selected).map(user => user.id)
        deleteUser(ids)
    }

    // if(users.length <= 0){
    //     return (
    //         <div style={{width: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
    //             <span>No users found</span>
    //         </div>
    //     )
    // }
  
    return (
      <>
      <div className='User-list'>
      <div style={{width: 600, overflow: 'auto'}}>
      <div style={{display: 'flex', justifyContent: 'space-between', alignItems: 'center'}}>
        <h3>User List</h3>
        <button style={{height: '20px'}} onClick={deleteBulkUsers}>Delete</button>
      </div>
      <table style={{width: '100%'}}>
        <thead>
        <tr>
            <th></th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email</th>
            <th>Created At</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
          {users.map(({id, first_name, last_name, email, created_at, selected}) => (
            <tr key={id}>
                <td>
                    <input type="checkbox" checked={selected}
                    onChange={e => toggleSelected(id)} />
                </td>
                <td>{first_name}</td>
                <td>{last_name}</td>
                <td>{email}</td>
                <td>{created_at}</td>
                <td>
                    <button onClick={(e) => deleteUser([id])}>Delete</button>
                </td>
            </tr>
          ))}
        </tbody>
      </table>
      </div>
      </div>
      <ImageViewer />
      </>
    )
  }