import { useState, useEffect, useRef } from "react"

export default function DrawBoard(props){
    const {imgUrl} = props
    const colors = ['red', 'blue', 'green', 'black', 'yellow', 'purple']
    const [selectedColor, setSelectedColor] = useState(colors[0])
    const canvasRef = useRef(null)
    const [context, setContext] = useState(null)
    const [isPainting, setIsPainting] = useState(false)
    const [canvasOffsetX, setCanvasOffsetX] = useState(null)
    const [canvasOffsetY, setCanvasOffsetY] = useState(null)
    const lineWidth = 5;
    const [startX, setStartX] = useState(0);
    const [startY, setStartY] = useState(0);
    const [startState, setStartState] = useState(null)

    const offset = 0

    useEffect(() => {
        const canvas = canvasRef.current
        setContext(canvas.getContext('2d'))
        setCanvasOffsetX(canvas.offsetLeft);
        setCanvasOffsetY(canvas.offsetTop);
        canvas.width = window.innerWidth - canvas.offsetLeft - 50;
        canvas.height = window.innerHeight - canvas.offsetTop - 50;
    }, [])
    
    useEffect(() => {
        if(context && canvasRef){
            setStartState(context.getImageData(0, 0, canvasRef.current.width, canvasRef.current.height))
            context.strokeStyle = selectedColor
        }
    }, [context])

    useEffect(() => {
        if(!context) return
        context.strokeStyle = selectedColor
    }, [selectedColor])

    function draw(e) {
        if(!isPainting) {
            return;
        }
    
        context.lineWidth = lineWidth;
        context.lineCap = 'round';
        context.lineTo(e.clientX - canvasOffsetX + offset + 60, e.clientY - canvasOffsetY + offset + 100);
        context.stroke();
    }

    function drawingStart(e) {
        setIsPainting(true);
        setStartX(e.clientX);
        setStartY(e.clientY);
    }
    
    function drawEnd(e){
        setIsPainting(false);
        context.stroke();
        context.beginPath();
    }

    function clear(){
        if(!context) return

        context.putImageData(startState, 0, 0);
    }

    function downloadSnap(){
        if(!canvasRef || !context) return;
        context.globalCompositeOperation="destination-over";
        const backgroundImage = new Image()
        backgroundImage.crossOrigin = "anonymous";
        backgroundImage.src = imgUrl

        backgroundImage.onload = function(){
            context.drawImage(backgroundImage,0,0, canvasRef.current.width, canvasRef.current.height);
            var link = document.createElement('a');
            link.download = 'drawing.png';
            link.href = canvasRef.current.toDataURL("image/png")
            link.click();
            clear()
        }
    }
    
    return (
        <div style={{width: '100%', display: 'flex', flexDirection: 'row'}}>
            <div style={{background: 'grey', display: 'flex', alignContent: 'center', flexDirection: 'column'}}>
                {colors.map(color => (
                    <div 
                    onClick={(e) => setSelectedColor(color)}
                    style={{background: color, height: 50, width: 50, margin: 5, border: `2px solid ${color === selectedColor ? 'white' : 'black'}`}}>
                    </div>
                ))}
                <button onClick={clear}>Erase</button>
                <button onClick={downloadSnap}>Download</button>
            </div>
            <div style={{flexGrow: 1, display: 'flex', justifyContent: 'center', alignContent: 'center'}}>
                <canvas 
                onMouseDown={drawingStart}
                onMouseUp={drawEnd}
                onMouseMove={draw}
                ref={canvasRef} 
                style={{border: '1px solid black', 
                ...(imgUrl ? {backgroundImage: `url(${imgUrl})`, backgroundPosition: 'center', backgroundSize: '100% 100%'} : {})
                }}
                />
            </div>
        </div>
    )
}