import './App.css';
import {useState, useEffect} from 'react'
import axios from './utils/axios'
import socketIOClient from "socket.io-client";
import {
  BrowserRouter as Router,
  Routes,
  Route
} from "react-router-dom"

//pages
import UserCreate from './pages/UserCreate';
import UserList from './pages/UserList';
import ImageViewer from './pages/ImageViewer';
import DrawBoard from './pages/DrawBoard';

function App() {
  const messageDuration = 2000
  const [users, setUsers] = useState([])
  const [showMessage, setShowMessage] = useState(false)
  const [message, setMessage] = useState('')

  useEffect(() => {
    fetchUsers()
    const socket = socketIOClient(process.env.REACT_APP_BASE_URL);
    socket.on("new-user", ({user}) => {
      addUser(user);
    });

    socket.on('users-deleted', data => {
      fetchUsers();
    })
  }, [])

  function addUser(user){
    setUsers(users => ([
      ...users,
      {...user, selected: false}
    ]))
  }

  function alert(message){
    setMessage(message)
    setShowMessage(true)

    setTimeout(() => {
      setShowMessage(false)
      setMessage('')
    }, messageDuration)
  }

  function fetchUsers(){
    axios.get(`/users`)
    .then(res => {
      setUsers(res?.data?.data?.users.map(user => ({
        ...user, selected: false
      })) || [])
    })
    .catch(err => console.log(err))
  }

  function saveUser(userData, callback){
    axios.post(`/user`, userData)
    .then(res => {
      alert(res?.data?.message || '')
    })
    .catch(err => console.log(err))
    .finally(() => {
      
      callback()
    })
  }

  function deleteUser(ids){
    if(!ids || ids.length <= 0) return
    axios.post(`/delete-users`, {ids})
    .then(res => {
      alert(res?.data?.message || '')
    })
    .catch(err => console.log(err))
    .finally(() => {

    })
  }

  function toggleSelected(id){
    setUsers(users => users.map(user => {
      if(user.id === id){
        return {...user, selected: !user.selected}
      }

      return user
    }))
  }

  return (
    <Router>
    <div className="App">
      <div style={{display: 'flex', height: '100%', justifyContent: 'center', alignItems: 'stretch', width: '100%'}}>
        <Routes>
          <Route exact path='/' element={
            <UserCreate 
              saveUser={saveUser}
            />
          }/>
          <Route exact path='/users' element={
            <UserList
              users={users}
              deleteUser={deleteUser}
              toggleSelected={toggleSelected}
            />
          }/>
          <Route exact path="/image" element={<ImageViewer alert={alert} />} />
          <Route exact path="/draw" element={<DrawBoard />} />
        </Routes>
      </div>
      {showMessage && <Message message={message} />}
    </div>
    </Router>
  );
}

function Message(props){
  const {message = "Something went wrong"} = props

  return (
    <div className='message'>
      <span>{message}</span>
    </div>
  )
}

export default App;
